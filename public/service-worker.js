/**
 * Welcome to your Workbox-powered service worker!
 *
 * You'll need to register this file in your web app and you should
 * disable HTTP caching for this file too.
 * See https://goo.gl/nhQhGp
 *
 * The rest of the code is auto-generated. Please don't update this file
 * directly; instead, make changes to your Workbox build configuration
 * and re-run your build process.
 * See https://goo.gl/2aRDsh
 */

importScripts("https://storage.googleapis.com/workbox-cdn/releases/4.3.1/workbox-sw.js");

self.addEventListener('message', (event) => {
  if (event.data && event.data.type === 'SKIP_WAITING') {
    self.skipWaiting();
  }
});

/**
 * The workboxSW.precacheAndRoute() method efficiently caches and responds to
 * requests for URLs in the manifest.
 * See https://goo.gl/S9QRab
 */
self.__precacheManifest = [
  {
    "url": "assets/css/0.styles.ce01bc4b.css",
    "revision": "5e7b0a499fbae79467952f4a068fe192"
  },
  {
    "url": "assets/img/404-page.a2835898.jpeg",
    "revision": "a2835898aa4341a0cb2f85ca36bfba36"
  },
  {
    "url": "assets/img/archive-page.a6857753.jpeg",
    "revision": "a685775332a2ad6ab2e8e619256e284e"
  },
  {
    "url": "assets/img/author-page.2b962f45.jpeg",
    "revision": "2b962f45fe1a7f1ba436c3a41d0b7318"
  },
  {
    "url": "assets/img/blog.45093621.jpeg",
    "revision": "45093621e2e18a7760d28c56e25843d0"
  },
  {
    "url": "assets/img/bt.0304c4eb.png",
    "revision": "0304c4eb4bee8d0cd80dbc785e66a387"
  },
  {
    "url": "assets/img/category-page.56121630.jpeg",
    "revision": "56121630a460e7f58d4ace0335b153a0"
  },
  {
    "url": "assets/img/custom-font.db770619.jpeg",
    "revision": "db77061950f4621b4cba389036851bc8"
  },
  {
    "url": "assets/img/dafolio-elementor.eacbf752.png",
    "revision": "eacbf7527f17d9f83fa0eb0398de41f2"
  },
  {
    "url": "assets/img/demo-1.24aef29d.png",
    "revision": "24aef29df0171e3f61ef817d6e85bb6e"
  },
  {
    "url": "assets/img/demo-2.ad8c1b10.png",
    "revision": "ad8c1b10ae0f2226e4339ad860185945"
  },
  {
    "url": "assets/img/demo-3.58136040.png",
    "revision": "58136040ed9dc35706bcbef8ef7da2a1"
  },
  {
    "url": "assets/img/demo-5.76ceb10c.png",
    "revision": "76ceb10c72cf95310ea69c666305bfad"
  },
  {
    "url": "assets/img/demo-7.b2c048e4.png",
    "revision": "b2c048e4665e5f51ed09483ff6bddc4b"
  },
  {
    "url": "assets/img/demo-8.8587ee13.png",
    "revision": "8587ee13d850e662de7969a4c909d0b6"
  },
  {
    "url": "assets/img/dn.c4b6e10b.png",
    "revision": "c4b6e10bdb3cac19cda27a72276a47f4"
  },
  {
    "url": "assets/img/elementor-setting.3054a609.png",
    "revision": "3054a60968b3888a8d5a34c7bba13e55"
  },
  {
    "url": "assets/img/footer.e6306229.png",
    "revision": "e630622933803a73a38bb56172504f1e"
  },
  {
    "url": "assets/img/header-1.379feeef.png",
    "revision": "379feeef0e9887e6106a83098c1cc9bc"
  },
  {
    "url": "assets/img/header-10.fa76c33d.png",
    "revision": "fa76c33d2d738938aa05e835e63dd443"
  },
  {
    "url": "assets/img/header-2.b5a23ebf.png",
    "revision": "b5a23ebf91910f638657b2e7c70a01c7"
  },
  {
    "url": "assets/img/header-3.cb66ffea.png",
    "revision": "cb66ffeab72037a62d3b2501db979b7c"
  },
  {
    "url": "assets/img/header-4.8a4bde6e.png",
    "revision": "8a4bde6e4086b93a75f96db34ed8d9d7"
  },
  {
    "url": "assets/img/header-5.01e70841.png",
    "revision": "01e7084135a19833bf1a80908a74c9bb"
  },
  {
    "url": "assets/img/ht.f4c8c03d.png",
    "revision": "f4c8c03d91423d0d17afb7ba6ad76b6e"
  },
  {
    "url": "assets/img/ie.d684fc29.png",
    "revision": "d684fc29af4f111c88237de691b32f45"
  },
  {
    "url": "assets/img/ins-1.119a3ded.png",
    "revision": "119a3ded4846f02ef94442f0237d3d71"
  },
  {
    "url": "assets/img/ins-2.3e54e3e0.png",
    "revision": "3e54e3e0a8a3bcca04331470aa7c7c14"
  },
  {
    "url": "assets/img/ins-33.a52e5524.png",
    "revision": "a52e55240f1250e5339772a046728dbc"
  },
  {
    "url": "assets/img/languge.3f054c43.jpeg",
    "revision": "3f054c434f127e59ac41f93db289acc6"
  },
  {
    "url": "assets/img/mit-5.b31a6088.png",
    "revision": "b31a60886f174be3268665da509b5e7f"
  },
  {
    "url": "assets/img/new-1.bb750626.png",
    "revision": "bb75062630a8e1a95bf9514c71873ba8"
  },
  {
    "url": "assets/img/ns1.48b74e47.png",
    "revision": "48b74e47e6bf993f6602df13b0ffb2bf"
  },
  {
    "url": "assets/img/pc.39e0805b.png",
    "revision": "39e0805bdd1cdacf5b30fed7f1114f8a"
  },
  {
    "url": "assets/img/portfolio-settings.b7436446.png",
    "revision": "b743644692619fa010b79ff1875fc64d"
  },
  {
    "url": "assets/img/preload-settings.7173fcd7.png",
    "revision": "7173fcd796580364cf3f3c4ec089a021"
  },
  {
    "url": "assets/img/search-page.e4350511.jpeg",
    "revision": "e4350511cd37b13a3258a6e0e7fcb638"
  },
  {
    "url": "assets/img/search.83621669.svg",
    "revision": "83621669651b9a3d4bf64d1a670ad856"
  },
  {
    "url": "assets/img/sidebar-settings.0edfdc2d.png",
    "revision": "0edfdc2d43a4462003b4deef07addabe"
  },
  {
    "url": "assets/img/single-post-page.664d0c8e.jpeg",
    "revision": "664d0c8e388923787fec3b00d4205551"
  },
  {
    "url": "assets/img/tag-page.1dc993cf.jpeg",
    "revision": "1dc993cf3606a264853b7117e630595a"
  },
  {
    "url": "assets/img/use-home1.a72b71cc.png",
    "revision": "a72b71cc8e12e3de8086b675478a6994"
  },
  {
    "url": "assets/img/voln3.7ce1ce07.png",
    "revision": "7ce1ce076a9545e9da3b0fe52a179f93"
  },
  {
    "url": "assets/js/1.f52f3901.js",
    "revision": "3611f3e32585da8e823bb15a9b6ab129"
  },
  {
    "url": "assets/js/10.d701402d.js",
    "revision": "17037bfa9d087cf1e272f0b181f7eaa8"
  },
  {
    "url": "assets/js/11.7c50b6f3.js",
    "revision": "fb183c71f9a7a557d656aba67a230d1b"
  },
  {
    "url": "assets/js/12.b9234c70.js",
    "revision": "0d97066cc1f909f95baae477c935cd58"
  },
  {
    "url": "assets/js/2.2eb93298.js",
    "revision": "66cc2f8dcb81228cab09ebb215d41486"
  },
  {
    "url": "assets/js/3.59a34dcd.js",
    "revision": "b7d59d1a625729918f026900555cb7e0"
  },
  {
    "url": "assets/js/4.8fdcf4e1.js",
    "revision": "217f04ef406876200dcf33e93e046e4d"
  },
  {
    "url": "assets/js/5.dfd4daa0.js",
    "revision": "deecccb54903ac8f1565ce084f41bb10"
  },
  {
    "url": "assets/js/6.7327576c.js",
    "revision": "6576073f5475c8850d40b856cee1f3e1"
  },
  {
    "url": "assets/js/7.ee6c26b5.js",
    "revision": "e3c7604fc5f5490afe45f36dfec96785"
  },
  {
    "url": "assets/js/8.1cb8eabc.js",
    "revision": "2a7b3efb56a01aa012cf603afc0ad7ac"
  },
  {
    "url": "assets/js/9.3a21b7b7.js",
    "revision": "a12f7cb72c00bd5a335deb031222a6e0"
  },
  {
    "url": "assets/js/app.57b0108f.js",
    "revision": "36323b03a3a6e6b39c60a43a1be9801c"
  },
  {
    "url": "component-example.html",
    "revision": "382f2c515f2291074db0a6eda6c80f3d"
  },
  {
    "url": "elementor/index.html",
    "revision": "3286f976bc75a973749e8590e204719c"
  },
  {
    "url": "favicon.png",
    "revision": "cf23526f451784ff137f161b8fe18d5a"
  },
  {
    "url": "hl2.png",
    "revision": "ff6f03f06d46e42ad19d31622933a3ef"
  },
  {
    "url": "icon-128x128.png",
    "revision": "26c1793143ce8977eba0618d2cc64d71"
  },
  {
    "url": "index.html",
    "revision": "b3fc02078a33099aec7b38165a7555d1"
  },
  {
    "url": "installation/index.html",
    "revision": "e3ae53e1063cdad15a0c15d1281a880c"
  },
  {
    "url": "logo.png",
    "revision": "3b8e3774e69e342b9f168d676413dc53"
  },
  {
    "url": "settings.jpg",
    "revision": "9e2ed7cc39c81c85dabe5a3fdc59cd61"
  },
  {
    "url": "settings/index.html",
    "revision": "40b4117bb5517a648903b2629e8a5db9"
  },
  {
    "url": "theme-options/index.html",
    "revision": "ca3a0f1662babaa032fd660997918025"
  },
  {
    "url": "vuepress-logo.png",
    "revision": "d1fed5cb9d0a4c4269c3bcc4d74d9e64"
  }
].concat(self.__precacheManifest || []);
workbox.precaching.precacheAndRoute(self.__precacheManifest, {});
addEventListener('message', event => {
  const replyPort = event.ports[0]
  const message = event.data
  if (replyPort && message && message.type === 'skip-waiting') {
    event.waitUntil(
      self.skipWaiting().then(
        () => replyPort.postMessage({ error: null }),
        error => replyPort.postMessage({ error })
      )
    )
  }
})
