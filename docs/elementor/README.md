
---


---
# Elementor Settings

After login, Go to `Dashbord`. Hover on `Elementor` button which is in leftside bar. And click on `Settings`.
- `Step 1` Checking this box will disable Elementor's Default Colors, and make Elementor inherit the colors from your theme.
- `Step 2`  Checking this box will disable Elementor's Default Fonts, and make Elementor inherit the fonts from your theme.

![Alt Text](./assets/images/wzd/elementor-setting.png)

## How to use the `Dafolio Elements` of Dafolio Theme?

These are the custom shortcodes of Dafolio Theme that you can use or edit these according to your needs. 

![Alt Text](./assets/images/wzd/dafolio-elementor.png)

## Set Up Home Page

- Go to Apperances -> Settings -> Reading
- Here you can select any `Home Page Style` by clicking on dropwond menu.

![Alt Text](./assets/images/wzd/use-home1.png)
