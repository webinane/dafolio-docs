---


---
# dafolio Options

## General Setting
- `Color Scheme` - You can select your theme's color here. by **Apperance** => **dafolio Options** => **General Settings** This color will be the main color.

![Alt Text](./assets/images/wzd/new-1.png)

###  Select Theme Skin

![Alt Text](./assets/images/wzd/theme-skin.png)
- Preloader Settings, Select the preloader type and add image or text for frontend.

![Alt Text](./assets/images/wzd/preload-settings.png)

- `Sidebar Source Type` Select Default Sidebar or chose from elementor.

![Alt Text](./assets/images/wzd/sidebar-settings.png)

## Header Setting
 - `Choose Header` Select the custom header as per your requirement.
 
 ![Alt Text](./assets/images/wzd/header-1.png)

### Header One Setting
- `Header One Setting` If you use header One style, just add your own logos and site banners etc..

 ![Alt Text](./assets/images/wzd/header-2.png)
### Header Two Setting
- `Header Two Setting`  If you use header Two style, just add your own logos and site banners etc.

 ![Alt Text](./assets/images/wzd/header-3.png)
 ### Header Three Setting
 - `Header Three Setting`  If you use header Three style, just add your own logos and site banners etc.

 ![Alt Text](./assets/images/wzd/header-4.png)
 ### Header Four Setting
- `Header Four Setting`  If you use header Four style, just add your own logos and site banners etc.

 ![Alt Text](./assets/images/wzd/header-5.png)
 

 ### Main Menu Typography
 - `Main Menu Typography` - This setting options will customize header style one and header style two Main Menu.

 ![Alt Text](./assets/images/wzd/header-10.png)
 - Enable to show menu bar typography for header both header styles.

## Footer
### Footer Settings
- `Style` - Select the custom footer as per your requirement.

![Alt Text](./assets/images/wzd/footer.png)

## Blog Setting

### Blog Page Settings

- `Blog Page Settings` You can set blog layouts by just clicking `ON` or `OFF` buttons below given the options of any blog sections or can change the text & images. 

![Alt Text](./assets/images/wzd/blog.jpeg)

### 404 Page Settings

- `404 Page Settings` You can set 404 page layout by just clicking `ON` or `OFF` buttons below given the options of any 404 page sections or can change the text & images. 

![Alt Text](./assets/images/wzd/404-page.jpeg)

### Author Page Settings

- `Author Page Settings` You can set Author page layout by just clicking `ON` or `OFF` buttons below given the options of any Author page sections or can change the text & images. 

![Alt Text](./assets/images/wzd/author-page.jpeg)

### Archive Page Settings

- `Archive Page Settings` You can set Archive page layout by just clicking `ON` or `OFF` buttons below given the options of any Archive page sections or can change the text & images. 

![Alt Text](./assets/images/wzd/archive-page.jpeg)

### Category Page Settings
- `Category Page Settings` You can set Category page layout by just clicking `ON` or `OFF` buttons below given the options of any Category page sections or can change the text & images. 

![Alt Text](./assets/images/wzd/category-page.jpeg)
 
### Tag Page Settings
- `Tag Page Settings` You can set Tag page layout by just clicking `ON` or `OFF` buttons below given the options of any Tag page sections or can change the text & images. 

![Alt Text](./assets/images/wzd/tag-page.jpeg)

### Search Page Settings
- `Search Page Settings` You can set Search page layout by just clicking `ON` or `OFF` buttons below given the options of any Search page sections or can change the text & images. 

![Alt Text](./assets/images/wzd/search-page.jpeg)

### Single Post Settings
- `Single Post Settings` You can set Single post layout by just clicking `ON` or `OFF` buttons below given the options.

![Alt Text](./assets/images/wzd/single-post-page.jpeg)


## Language Settings
- First Select your langue and then upload .mo language file.
![Alt Text](./assets/images/wzd/languge.jpeg)

## Custom Font Settings
- Please upload your desire font file in *.ttf, *.otf, *.eot, *.woff format.
![Alt Text](./assets/images/wzd/custom-font.jpeg)

## Typography Settings
These are the options for any change in heading's typography. If you are not satisfied with default heading typography then you can easily change you desired typo.

![Alt Text](./assets/images/wzd/ht.png)

### Body Typography Settings
Enable to customize the theme Apply options to customize the body,paragraph fonts for the theme.

![Alt Text](./assets/images/wzd/bt.png)

## Import/Export

### Import Options
Suppose if have two websites and want same theme options then you should go on old website  and download or export theme options with exporter. Now go to new website importer and upload the file. After uploading you will see both websites have same theme options. 

### Export Options
Here you can copy/download your current option settings. Keep this safe as you can use it as a backup should anything go wrong, or you can use it to restore your settings on this site (or any other site).

![Alt Text](./assets/images/wzd/ie.png)
