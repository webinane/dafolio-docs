---


---


# Post Types
Common Settings:- Here you will see common post meta settings. All post types have same meta.
**Header Settings:**
- `Header Style` - You will see only one option here which is "Header style". If you want to set every page with different headers the you should this option.


**Title Settings:**
- `Show Title Section` - Enable to show title banner section on this page.
- `Header Banner Custom Title` - Enter the custom title for header banner section
- `Show Breadcrumb` - Click on check box if you want to show breadcrumb.
- `Title Section Background` - Upload background image for page title section.

**Sidebar Layout:**
- `Sidebar Layout` - You can se main content and sidebar alignment with this option.




## Testimonial
- - Go to"**Testimonial**" => "**Add New**".
- Add your title and explaination to the given places.

![Alt Text](../assets/images/wzd/ns1.png)
- Excerpts are optional hand-crafted summaries of your content that can be used in your theme.

![Alt Text](../assets/images/wzd/ns9.png)

- You can also insert the **featured image** of your service.

![Alt Text](../assets/images/wzd/ns11.png)


## Portfolio
- Go to"Portfolio" => "Add New".
Add your title and explaination to the given places.

![Alt Text](../assets/images/wzd/ns1.png)
- Excerpts are optional hand-crafted summaries of your content that can be used in your theme.

![Alt Text](../assets/images/wzd/ns9.png)
- Change or add custom settings in Portfolio.

![Alt Text](../assets/images/wzd/dn.png)

- You can add **category** from the given section.

![Alt Text](../assets/images/wzd/pc.png)
- You can also insert the **featured image** of your Portfolio.

![Alt Text](../assets/images/wzd/ns11.png)
- You can also insert the **Portfolio Settings** of your Portfolio.

![Alt Text](../assets/images/wzd/portfolio-settings.png)


## Team 
 - **Add Team Members**
 
 - Go to"**Team**" => "**Add New**"

Add your title and explaination to the given places.

![Alt Text](../assets/images/wzd/ns1.png)

- Change or add custom settings in Portfolio.

![Alt Text](../assets/images/wzd/dn.png)

- Team Settings.

![Alt Text](../assets/images/wzd/voln3.png)

 
## Services
- Go to"**Services**" => "**Add New**".
- Add your title and explaination to the given places.

![Alt Text](../assets/images/wzd/ns1.png) 

- Change or add custom settings in Portfolio.

![Alt Text](../assets/images/wzd/dn.png)
- Add Services Icon

![Alt Text](../assets/images/wzd/services-icon.png)
